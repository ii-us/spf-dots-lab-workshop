using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    public GameObject prefab;
    public float spawnRate = 0.01f;
    float nextSpawnTime = 0;

    void Update()
    {
        if (nextSpawnTime < Time.realtimeSinceStartup) {
            Instantiate(prefab, this.transform.position, Quaternion.identity);
            nextSpawnTime = Time.realtimeSinceStartup + spawnRate;
        }
    }
}
