using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    Vector3 movementDirection;
    float movementSpeed;

    void Start()
    {
        movementDirection = Random.insideUnitSphere;
        movementSpeed = Random.Range(5.0f, 10.0f);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += movementDirection * movementSpeed * Time.deltaTime;
        if (movementSpeed > 0)
        {
            movementSpeed -= 1 * Time.deltaTime;
        }
        else {
            movementSpeed = 0;
        }
    }
}
